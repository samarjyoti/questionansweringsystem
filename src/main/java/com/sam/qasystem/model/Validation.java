package com.sam.qasystem.model;

import javax.xml.bind.annotation.XmlElement;

public class Validation {
	
	private String type;
	
	private String value;
	
	private String condition;
	
	private String verificationRequired;

	@XmlElement
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlElement
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	@XmlElement
	public String getVerificationRequired() {
		return verificationRequired;
	}

	public void setVerificationRequired(String verificationRequired) {
		this.verificationRequired = verificationRequired;
	}
	
	

}
