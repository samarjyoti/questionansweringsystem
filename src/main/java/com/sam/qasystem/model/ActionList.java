package com.sam.qasystem.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "actionlist")
public class ActionList {

	private List<Action> actions = new ArrayList<Action>();

	@XmlElement(name = "action", type = Action.class)
	public List<Action> getActionList() {
		return actions;
	}

	public void setActionList(List<Action> actionList) {
		this.actions = actionList;
	}
	
	public Action getAction(int id) {
		for(Action action: actions) {
			if(action.getId() == id) {
				return action;
			}
		}
		return null;
	}

}
