package com.sam.qasystem.model;

import javax.xml.bind.annotation.XmlAttribute;

public class Answer {

	private String value;
	private int consequence;

	
	@XmlAttribute
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute
	public int getConsequence() {
		return consequence;
	}

	public void setConsequence(int consequence) {
		this.consequence = consequence;
	}

}
