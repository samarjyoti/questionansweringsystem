package com.sam.qasystem.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class AnswerList {

	private List<Answer> answerList;

	@XmlElement(name = "answer", type = Answer.class)
	public List<Answer> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<Answer> answerList) {
		this.answerList = answerList;
	}

	public int getNextAnswerId(String value) {
		for (Answer a : answerList) {
			if (a.getValue().equalsIgnoreCase(value)) {
				return a.getConsequence();
			}
		}
		return 0;
	}
}
