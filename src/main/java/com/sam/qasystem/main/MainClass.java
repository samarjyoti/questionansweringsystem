package com.sam.qasystem.main;

import java.io.File;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.sam.qasystem.model.ActionList;

public class MainClass {

	public static void main(String[] args) {

		ActionList list = null;
		try {
			URL fileUrl = MainClass.class.getResource("/QuestionConfig.xml");
			File file = new File(fileUrl.getFile());
			JAXBContext jaxbContext = JAXBContext.newInstance(ActionList.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			list = (ActionList) jaxbUnmarshaller.unmarshal(file);
			// System.out.println(list);

		} catch (JAXBException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		QASystem.runSystem(list);

	}

}
