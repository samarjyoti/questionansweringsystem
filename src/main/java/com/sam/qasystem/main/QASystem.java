package com.sam.qasystem.main;

import java.util.Random;
import java.util.Scanner;

import com.sam.qasystem.model.Action;
import com.sam.qasystem.model.ActionList;
import com.sam.qasystem.model.User;
import com.sam.qasystem.model.Validation;

public class QASystem {

	static User user = new User();

	public void loadConfigurations(String file) {

	}

	public static void runSystem(ActionList actionList) {

		Action nextAction = actionList.getActionList().get(0);

		Scanner scanner = new Scanner(System.in);
		while (true) {

			Action action = nextAction;

			if (action.getContentList() != null) {
				for (String str : action.getContentList()) {
					System.out.println(str);
				}
			}

			if (action.getGenerateType() != null) {

				if (action.getGenerateType().getValue().equals("OTP")) {
					int length = action.getGenerateType().getLength();
					Random rand = new Random();

					int rand_int1 = rand.nextInt((int) Math.pow(10, length + 1));
					String otp = String.valueOf((int) (Math.random() * 9000) + 1000);
					user.setOTP(otp);
					System.out.println("**Hint** OTP is:" + otp);

					nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("success"));
				} else if (action.getGenerateType().getValue().equals("Balance")) {
					System.out.println("Your balance is " + user.getBalance() + " Rs.");
					if (validate(action.getValidation(), String.valueOf(user.getBalance()))) {
						nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("success"));
					} else {
						nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("failure"));
					}
				} else {
					nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("success"));
				}

			} else {
				String input = scanner.nextLine();

				if ("TextField".equals(action.getType())) {

					if (validate(action.getValidation(), input)) {
						nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("success"));
					} else {
						nextAction = actionList.getAction(action.getOutcomeList().getNextOutcomeId("failure"));
					}
				} else {
					nextAction = actionList.getAction(action.getAnswerList().getNextAnswerId(input));
				}
			}

		}
	}

	public static boolean validate(Validation validation, String ip) {

		if ("mobileNumber".equals(validation.getType())) {

			if (ip.matches("\\d{" + validation.getValue() + "}")) {
				user.setMobileNumber(ip);
				return true;
			}

		} else if ("OTP".equals(validation.getType())) {
			if (user.getOTP().equals(ip) && ip.matches("\\d{" + validation.getValue() + "}"))
				return true;
		} else if ("Balance".equals(validation.getType())) {
			if (validation.getCondition().equals("lt")) {
				if (Integer.parseInt(ip) < Integer.parseInt(validation.getValue()))
					return true;
			}
			if (validation.getCondition().equals("gte")) {
				if (Integer.parseInt(ip) > Integer.parseInt(validation.getValue()))
					return true;
			}
		} else if ("Recharge".equals(validation.getType())) {
			if (validation.getCondition().equals("lt")) {
				if (Integer.parseInt(ip) < Integer.parseInt(validation.getValue())) {
					user.setBalance(user.getBalance() + Integer.parseInt(ip));
					return true;
				}
			}
			if (validation.getCondition().equals("gte")) {
				if (Integer.parseInt(ip) > Integer.parseInt(validation.getValue()))
					return true;
			}
		}
		return false;
	}

}
