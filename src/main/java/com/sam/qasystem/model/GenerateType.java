package com.sam.qasystem.model;

import javax.xml.bind.annotation.XmlAttribute;

public class GenerateType {

	private String value;

	private int length;

	@XmlAttribute
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
