package com.sam.qasystem.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class OutcomeList {

	
	private List<Outcome> outcomes;

	@XmlElement(name = "outcome", type = Outcome.class)
	public List<Outcome> getOutcomes() {
		return outcomes;
	}

	public void setOutcomes(List<Outcome> outcomes) {
		this.outcomes = outcomes;
	}
	
	public int getNextOutcomeId(String value) {
		for(Outcome o:outcomes) {
			if(o.getValue().equals(value)) {
				return o.getConsequence();
			}
		}
		return 0;
	}
}
