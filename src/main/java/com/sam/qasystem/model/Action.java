package com.sam.qasystem.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Action {

	private int id;

	private String type;

	private List<String> contentList = new ArrayList<>();

	private OutcomeList outcomeList;

	private AnswerList answerList;

	private Validation validation;

	private boolean input;
	
	private GenerateType generateType;

	@XmlElement
	public GenerateType getGenerateType() {
		return generateType;
	}

	public void setGenerateType(GenerateType generateType) {
		this.generateType = generateType;
	}

	@XmlAttribute
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlAttribute
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "outcomeList")
	public OutcomeList getOutcomeList() {
		return outcomeList;
	}

	public void setOutcomeList(OutcomeList outcomeList) {
		this.outcomeList = outcomeList;
	}

	@XmlElement(name = "answerList")
	public AnswerList getAnswerList() {
		return answerList;
	}

	public void setAnswerList(AnswerList answerList) {
		this.answerList = answerList;
	}

	
	@XmlElementWrapper(name="ContentList")
    @XmlElement(name = "list")
	public List<String> getContentList() {
		return contentList;
	}

	public void setContentList(List<String> contentList) {
		this.contentList = contentList;
	}

	@XmlElement
	public Validation getValidation() {
		return validation;
	}

	public void setValidation(Validation validation) {
		this.validation = validation;
	}

	@XmlElement
	public boolean getInput() {
		return input;
	}

	public void setInput(boolean input) {
		this.input = input;
	}

}
