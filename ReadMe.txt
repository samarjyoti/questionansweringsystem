Design:

Basic design of the application is to make the question flow(user journey) configurable by defining the questions in XML file(QuestionConfig.xml). All questions and the flow is captured from this xml and this can be further modified to change the user journey as required. Though, there is still much scope to make the application more configurable, i have tried to make it as much configurable as possible within short span of time.


Instruction to execute:

The application is created using maven. To set up please clone and import into eclipse as existing maven project. To run the application, run the class MainClass.java as java program.
